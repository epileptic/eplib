package de.epileptic.eplib.clipgrab.test;

import org.junit.jupiter.api.Test;

import de.epileptic.eplib.clipgrab.ClipBoardListener;
import de.epileptic.eplib.clipgrab.ClipGrab;
import de.epileptic.eplib.clipgrab.ClipGrabController;

class ClipGrabTest {

	@Test
	void test() {
		ClipGrab grab = ClipGrabController.createClipGrab();
		grab.addClipboardListener(new ClipBoardListener() {

			@Override
			public void onClipboardChanged(String newContent, String oldContent) {
				System.out.println(newContent);
				System.out.println(oldContent);
			}

		});
	}

}
