package de.epileptic.eplib.clipgrab;

/**
 * Listener Interface which can be registered on {@link ClipGrab}. An object of
 * that can be obtained by creating one with the {@link ClipGrabController}.
 * 
 * @author epileptic
 * @version 1.0
 * @since beta-1.4
 */
public interface ClipBoardListener {
	public void onClipboardChanged(String newContent, String oldContent);
}
