package de.epileptic.eplib.clipgrab;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A class implementing {@link Runnable} which will check every 100 ms for
 * clipboard change. In cas of a change all listeners will be notified.
 * 
 * @author epileptic
 * @version 1.0
 * @since beta-1.4
 */
public class ClipGrab implements Runnable {

	private boolean running = true;

	private CopyOnWriteArrayList<ClipBoardListener> listener;

	ClipGrab() {
		listener = new CopyOnWriteArrayList<ClipBoardListener>();
	}

	@Override
	public void run() {
		String lastClip = "";
		String clip = "";
		while (running) {
			try {
				clip = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);

				if (!clip.equals(lastClip))
					for (ClipBoardListener i : listener)
						i.onClipboardChanged(clip, lastClip);
			} catch (HeadlessException e) {
				e.printStackTrace();
			} catch (UnsupportedFlavorException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			lastClip = clip;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * Registers a {@link ClipBoardListener}.
	 * 
	 * @param listener {@link ClipBoardListener} that has to be registered.
	 */
	public void addClipboardListener(ClipBoardListener listener) {
		this.listener.add(listener);
	}

	/**
	 * Unregisters a {@link ClipBoardListener}
	 * 
	 * @param listener {@link ClipBoardListener} that has to be unregistered
	 */
	public void removeClipboardListenet(ClipBoardListener listener) {
		this.listener.remove(listener);
	}

	/**
	 * Stops the while loop which leads the thread this {@link ClipGrab} is running
	 * in to stop.
	 */
	public void stop() {
		running = false;
	}
}
