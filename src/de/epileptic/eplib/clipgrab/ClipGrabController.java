package de.epileptic.eplib.clipgrab;

import java.util.ArrayList;

/**
 * Factory class for {@link ClipGrab}.
 * 
 * @author epileptic
 * @version 1.0
 * @since beta-1.4
 */
public class ClipGrabController {

	private static ArrayList<ClipGrab> grabber;
	private static ArrayList<Thread> grabberThreads;

	private ClipGrabController() {
	}

	/**
	 * Returns a newly created instance of {@link ClipGrab}. It already got started
	 * in a separate thread.
	 * 
	 * @return new instance of {@link ClipGrab}
	 */
	public static ClipGrab createClipGrab() {
		if (grabber == null)
			grabber = new ArrayList<ClipGrab>();

		if (grabberThreads == null)
			grabberThreads = new ArrayList<Thread>();

		ClipGrab grab = new ClipGrab();
		Thread current = new Thread(grab);
		current.start();

		grabber.add(grab);
		grabberThreads.add(current);

		return grab;
	}

	/**
	 * Stops all running {@link ClipGrab} instances.
	 */
	public static void stopAll() {
		for (ClipGrab g : grabber)
			g.stop();
	}

}
