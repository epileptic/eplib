package de.epileptic.eplib.log;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import de.epileptic.eplib.Registry;

/**
 * A logging daemon that is configured by the {@link Registry}.
 * 
 * @author epileptic
 * @version 1.1
 * @since beta-1.0
 */
public class Log {
	// Stream remapping
	private static final PrintStream ERR = System.err;
	private static final PrintStream OUT = System.out;

	// Logfile
	private static File logFile = Registry.getInstance().getLogFile();
	private static PrintWriter logFileWriter;

	/**
	 * Prints a Log message to the log file and to console if verbose is active.
	 * Verbose and file logging could be enabled in {@link Registry}.
	 * 
	 * @param message {@link String} message that has to be logged
	 * @param source  {@link Object} that sent the message
	 */
	public static void log(String message, Object source) {
		print(OUT, smartValueSource(source), message);
	}

	/**
	 * Prints a warning log message to the log file and to console if verbose is
	 * active. Verbose and file logging could be enabled in {@link Registry}.
	 * 
	 * @param message {@link String} message that has to be logged
	 * @param source  {@link Object} that sent the message
	 */
	public static void logWarn(String message, Object source) {
		print(ERR, smartValueSource(source), message);
	}

	/**
	 * Prints a exception log message to the log file and to console if verbose is
	 * active. Verbose and file logging could be enabled in {@link Registry}.
	 * 
	 * @param message {@link String} message that has to be logged
	 * @param source  {@link Object} that sent the message
	 */
	public static void logException(Exception e) {
		StringBuilder sb = new StringBuilder(e.getClass().getSimpleName() + " : " + e.getMessage());

		for (StackTraceElement i : e.getStackTrace()) {
			sb.append("\n" + stamp() + ">> " + i);
		}

		print(ERR, "EXCEPTION", sb.toString());
	}

	private static void print(PrintStream os, String source, String message) {
		// Log to file
		try {
			if (logFileWriter != null && logFile != null) {
				if (!logFile.exists()) {
					logFile.getParentFile().mkdirs();
					logFile.createNewFile();
				}
				logFileWriter = new PrintWriter(logFile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Check if verbose. If not : return
		switch (Registry.getInstance().getLogLevel()) {
		case SILENT:
			return;

		case ERROR:
			if (os == OUT)
				return;

		default:
			break;
		}

		// Message builder
		StringBuilder sb = new StringBuilder();
		sb.append(stamp());
		sb.append(source + " > ");
		sb.append(message);

		// Print
		os.println(sb.toString());
	}

	/**
	 * Creates ISO-8601 timestamp.
	 * 
	 * @return timestamp as {@link String}
	 */
	public static String stamp() {
		String stamp = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		StringBuilder sb = new StringBuilder("[ ");
		sb.append(stamp);
		while (sb.length() < 25)
			sb.append("0");
		sb.append(" ] ");

		return sb.toString();
	}

	private static String smartValueSource(Object source) {
		if (source instanceof String)
			return (String) source;
		else
			return source.getClass().getSimpleName();
	}
}
