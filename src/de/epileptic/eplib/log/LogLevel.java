package de.epileptic.eplib.log;

public enum LogLevel {
	VERBOSE, ERROR, SILENT
}
