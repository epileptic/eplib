package de.epileptic.eplib.tools;

import java.io.File;
import java.util.Properties;

public class SchroedingersBox {
	@SuppressWarnings("unchecked")
	public static <T> T getKeyOrDefault(Properties prp, String key, T defaultValue) {
		T retVal = null;
		try {
			retVal = (T) prp.get(key);
		} catch (Exception e) {
		} finally {
			if (retVal == null)
				retVal = defaultValue;
		}
		return retVal;
	}

	public static File getFileOrNull(String path) {
		File retVal = null;
		try {
			if (path != null)
				retVal = new File(path);
		} catch (Exception e) {
		}
		return retVal;
	}
}
