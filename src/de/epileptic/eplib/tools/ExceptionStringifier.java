package de.epileptic.eplib.tools;

public class ExceptionStringifier {
	private ExceptionStringifier() {
	}

	public static String getTypeAndMessage(Exception e) {
		return e.getClass().getSimpleName() + ":" + e.getMessage();
	}
}
