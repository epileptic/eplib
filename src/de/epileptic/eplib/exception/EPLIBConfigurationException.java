package de.epileptic.eplib.exception;

public class EPLIBConfigurationException extends RuntimeException {

	private static final long serialVersionUID = 1771486881836687608L;

	public EPLIBConfigurationException(Exception e) {
		super(e);
	}

}
