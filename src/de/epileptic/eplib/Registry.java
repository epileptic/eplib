package de.epileptic.eplib;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Properties;

import de.epileptic.eplib.log.LogLevel;
import de.epileptic.eplib.tools.SchroedingersBox;

public class Registry {
	static Registry instance;

	// GENERAL
	public final String DEFAULT_ENCODING;

	// COMMANDS
	public static final String REQUEST_COMMAND = "request";
	public static final String RESPOND_COMMAND = "respond";

	// AUTH
	public final String AUTH_HASH_ALGORITHM;
	public final int AUTH_HASH_LENGTH;
	public final double AUTH_HASH_TIMEOUT;

	// EXCEPTION MESSAGES
	public static final String INPUT_TOO_SMALL = "Input data too small!";
	public static final String UNDEFINED_SERIALIZER = "Serializer undefined for class ";
	public static final String CHEKSUM_DIFFER = "Checksum differ! ";
	public static final String DIFFERENT_PROTOCOL = "Peer uses different protocol. ";
	public static final String HASH_COMPARE_TIMEOUT_MESSAGE = "Timeout. ";

	// LOG
	private File logFile = null;
	private LogLevel logLevel;

	// Locks
	HashMap<Object, String> locks = new HashMap<Object, String>();

	private Registry() {
		File cfg = new File("eplib_config.cfg");
		Properties prp = new Properties();
		try {
			prp.load(new FileInputStream(cfg));
		} catch (Exception e) {
		}

		DEFAULT_ENCODING = SchroedingersBox.getKeyOrDefault(prp, "DEFAULT_ENCODING", "UTF-8");
		AUTH_HASH_ALGORITHM = SchroedingersBox.getKeyOrDefault(prp, "AUTH_HASH_ALGORITHM", "MD5");
		AUTH_HASH_LENGTH = SchroedingersBox.getKeyOrDefault(prp, "AUTH_HASH_LENGTH", 16);
		AUTH_HASH_TIMEOUT = SchroedingersBox.getKeyOrDefault(prp, "AUTH_HASH_TIMEOUT", 1000D);
		logLevel = SchroedingersBox.getKeyOrDefault(prp, "LOG_VERBOSE", LogLevel.VERBOSE);
		logFile = SchroedingersBox.getFileOrNull(SchroedingersBox.getKeyOrDefault(prp, "LOG_FILE", null));
	}

	public static synchronized Registry getInstance() {
		if (instance == null)
			instance = new Registry();
		return instance;
	}

	public LogLevel getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}

	public File getLogFile() {
		return logFile;
	}

	public boolean defineLogFile(File logFile) {
		if (logFile == null) {
			this.logFile = logFile;
			return true;
		}
		return false;
	}

	public boolean isLogToLogfile() {
		return logFile != null;
	}

}
