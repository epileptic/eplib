package de.epileptic.eplib.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

import de.epileptic.eplib.log.Log;

/**
 * Class for simple persistent configuration management.
 * 
 * @author epileptic
 * @version 1.0
 * @since beta-1.3
 */
public class GlobalSettings {
	// Runtime controls
	private static boolean shutdownHookAdded = false;
	private static boolean configFileSet = false;

	// Properties
	private static Properties persistent = new Properties();

	// Configuration file
	private static File configFile;

	/**
	 * Sets the configuration {@link File} and loads it content to runtime
	 * {@link Properties}. Also adds a shutdown hook for automatic file safe if not
	 * done yet.
	 * 
	 * @param config {@link File} source for runtime {@link Properties}
	 * @throws IOException if IOError occurs
	 */
	public static void setConfigFile(File config) throws IOException {
		if (!Files.exists(config.toPath()))
			Files.createFile(config.toPath());

		configFile = config;
		persistent.load(new FileInputStream(config));
		configFileSet = true;

		if (!shutdownHookAdded)
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					GlobalSettings.savePersistence();
				}
			}));
	}

	/**
	 * Gets a value from the configuration.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return value of key if existent otherwise defaultValue
	 */
	public static String get(String key, String defaultValue) {
		if (!persistent.containsKey(key))
			persistent.setProperty(key, defaultValue);
		return persistent.getProperty(key);
	}

	/**
	 * Saves configuration to file.
	 */
	public static void savePersistence() {
		try {
			if (configFileSet)
				persistent.store(new FileWriter(configFile), GlobalSettings.class.getSimpleName());
		} catch (IOException e) {
			Log.logException(e);
		}
	}
}