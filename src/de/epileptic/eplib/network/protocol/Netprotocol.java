package de.epileptic.eplib.network.protocol;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.exception.EPLIBConfigurationException;
import de.epileptic.eplib.network.packer.PackageDeclaration;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;
import de.epileptic.eplib.network.serialization.serializer.ByteSerializer;
import de.epileptic.eplib.network.serialization.serializer.IntSerializer;
import de.epileptic.eplib.network.serialization.serializer.PackageDeclarationSerializer;
import de.epileptic.eplib.network.serialization.serializer.StringSerializer;

/**
 * Extend this to create a sub protocol. It contains basic data defining the
 * package header.
 * 
 * @author Epileptic
 *
 */
public abstract class Netprotocol {
	LinkedHashMap<Class<?>, Serializer<?>> serializer = new LinkedHashMap<Class<?>, Serializer<?>>();
	ArrayList<Serializer<?>> indices = new ArrayList<Serializer<?>>();

	public Netprotocol() {
		registerSerializer(new ByteSerializer(), byte[].class);
		registerSerializer(new StringSerializer(), String.class);
		registerSerializer(new PackageDeclarationSerializer(), PackageDeclaration.class);
		registerSerializer(new IntSerializer(), Integer.class);
	}

	/**
	 * Registers a {@link Serializer} to a specific {@link Class}. This will be used
	 * to serialize every Object of a specific {@link Class}.
	 * 
	 * @param s {@link Serializer} which will be used to serialize Objects of the
	 *          {@link Class} c
	 * @param c {@link Class} that will be serialized by s
	 */
	protected final <T> void registerSerializer(Serializer<T> s, Class<T> c) {
		serializer.put(c, s);
		indices.add(s);
	}

	/**
	 * Defines the protocol name. Only used to compare end point protocols.
	 * 
	 * @return {@link String} name of the protocol
	 */
	public abstract String getName();

	/**
	 * Defines the current version of this protocol. Only used to compare end point
	 * protocols.
	 * 
	 * @return double version of the protocol
	 */
	public abstract double getVersion();

	/**
	 * Defines the custom header size for this protocol.
	 * 
	 * @return int header size
	 */
	public abstract int getHeaderSize();

	/**
	 * Defines how long the data size declaration is. 4 bytes for int.
	 * 
	 * @return int length of data size declaration.
	 */
	public abstract int getDataLengthSize();

	/**
	 * Defines how many bytes the checksum takes. MD5 takes 16.
	 * 
	 * @return int length of checksum
	 */
	public abstract int getChecksumSize();

	/**
	 * Defines checksum algorithm used to check data validity. Used to get
	 * {@link MessageDigest} instance by algorithm name.
	 * 
	 * @return {@link String} checksum algorithm to use.
	 */
	public abstract String getChecksumAlgorithm();

	/**
	 * Generates a protocol MD5 hash from all its parameter to check integrity with
	 * peer.
	 * 
	 * @return byte[] containing the MD5 hash
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public final byte[] getProtocolHash() {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance(Registry.getInstance().AUTH_HASH_ALGORITHM);
			md.update(getName().getBytes(Registry.getInstance().DEFAULT_ENCODING));
			md.update(ByteBuffer.allocate(8).putDouble(getVersion()).array());
			md.update(ByteBuffer.allocate(4).putInt(getHeaderSize()).array());
			md.update(ByteBuffer.allocate(4).putInt(getDataLengthSize()).array());
			md.update(ByteBuffer.allocate(4).putInt(getChecksumSize()).array());
			md.update(getChecksumAlgorithm().getBytes(Registry.getInstance().DEFAULT_ENCODING));

			for (Serializer<?> s : indices) {
				md.update(s.getClass().getName().getBytes(Registry.getInstance().DEFAULT_ENCODING));
			}

			return md.digest();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new EPLIBConfigurationException(e);
		}
	}

	/**
	 * Creates a clone of the {@link Class}/{@link Serializer}
	 * {@link LinkedHashMap}. Method for read only purposes.
	 * 
	 * @return {@link LinkedHashMap} clone of the {@link Class}/{@link Serializer}
	 *         map.
	 */
	@SuppressWarnings("unchecked")
	public final LinkedHashMap<Class<?>, Serializer<?>> getClassSerializerClone() {
		return (LinkedHashMap<Class<?>, Serializer<?>>) serializer.clone();
	}

	/**
	 * Creates a clone of the {@link Serializer} {@link ArrayList}. Method for read
	 * only purposes.
	 * 
	 * @return {@link ArrayList} containing all Serializer
	 */
	@SuppressWarnings("unchecked")
	public final ArrayList<Serializer<?>> getSerializerClone() {
		return (ArrayList<Serializer<?>>) indices.clone();
	}

	/**
	 * Serializes passed {@link Object} by searching assigned {@link Serializer} and
	 * call serialize on it.
	 * 
	 * @param o Object that has to be serialized
	 * @return {@link SerializedObject}
	 * @throws SerializationException if no {@link Serializer} is defined for the
	 *                                {@link Class} of o
	 */
	@SuppressWarnings("unchecked")
	public final <T> SerializedObject serialize(T o) throws SerializationException {
		if (!serializer.containsKey(o.getClass()))
			throw new SerializationException(Registry.UNDEFINED_SERIALIZER);
		Serializer<T> z = (Serializer<T>) serializer.get(o.getClass());
		return new SerializedObject(ByteBuffer.allocate(4).putInt(indices.indexOf(z)).array(), z.serialize(o));
	}

	/**
	 * Deserializes passed {@link SerializedObject} by searching assigned.
	 * 
	 * {@link Serializer} and call deserialize on it.
	 * 
	 * @return {@link Object} deserialized Object
	 * @throws SerializationException if no {@link Serializer} is defined for this
	 *                                {@link Class}.
	 */
	@SuppressWarnings("unchecked")
	public final <T> T deserialize(SerializedObject o) throws SerializationException {
		Serializer<T> z;
		try {
			z = (Serializer<T>) indices.get(ByteBuffer.wrap(o.getIdentifier()).getInt());
		} catch (ClassCastException | NullPointerException e) {
			throw new SerializationException(Registry.UNDEFINED_SERIALIZER + o.getClass().getName());
		}
		return z.deserialize(o, this);
	}
}
