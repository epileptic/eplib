package de.epileptic.eplib.network.protocol.exceptions;

public class EPLIBProtocolException extends RuntimeException {
	private static final long serialVersionUID = 8265935811066712627L;

	public EPLIBProtocolException(Exception e) {
		super(e);
	}
}
