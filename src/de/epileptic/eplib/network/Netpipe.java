package de.epileptic.eplib.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.log.Log;
import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.protocol.exceptions.EPLIBProtocolException;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;

public class Netpipe {
	// Protocol
	private Netprotocol protocol;

	// Network things
	private InputStream inputStream;
	private OutputStream outputStream;

	/**
	 * Constructs a {@link Netpipe} Object. It verifies the {@link Netprotocol}
	 * through the Streams given by parameter.
	 * 
	 * @param inputStream {@link InputStream} used to send to
	 * @param ouputStream {@link OutputStream} used to retrieve from
	 * @param protocol    {@link Netprotocol} used for communication
	 * 
	 * @throws IOException              if an I/O error occurs
	 * @throws NoSuchAlgorithmException if protocol hash generating fails because of
	 *                                  missing MD5 algorithm
	 * @throws SerializationException   if the peer protocol is different from the
	 *                                  local
	 * @throws AuthenticationException  if the hash compare runs into the timeout
	 */
	public Netpipe(InputStream inputStream, OutputStream ouputStream, Netprotocol protocol)
			throws IOException, SerializationException, AuthenticationException {
		this.inputStream = inputStream;
		this.outputStream = ouputStream;
		this.protocol = protocol;

		// Send protocol hash
		outputStream.write(protocol.getProtocolHash());
		outputStream.flush();
		Log.log("protocol hash generated and sent! awaiting response...", this);

		// Await bytes in stream
		double startTime = System.currentTimeMillis();
		while (inputStream.available() < Registry.getInstance().AUTH_HASH_LENGTH) {
			
			if ((System.currentTimeMillis() - startTime) > Registry.getInstance().AUTH_HASH_TIMEOUT)
				throw new AuthenticationException(Registry.HASH_COMPARE_TIMEOUT_MESSAGE);
		}

		// Read hash
		byte[] hash = new byte[Registry.getInstance().AUTH_HASH_LENGTH];
		inputStream.read(hash);

		if (!Arrays.equals(hash, protocol.getProtocolHash()))
			throw new SerializationException(Registry.DIFFERENT_PROTOCOL);

		Log.log("verified!", this);
	}

	/**
	 * Sends the passed {@link Object} through the Socket given by constructor. The
	 * passed object will be serialized by the corresponding {@link Serializer}.
	 * 
	 * @param o {@link Object} that has to be sent
	 * @throws SerializationException   if no {@link Serializer} is defined for o's
	 *                                  {@link Class}
	 * @throws NoSuchAlgorithmException if the {@link Netprotocol} returns a
	 *                                  checksum algorithm which does not exist
	 * @throws IOException              if an I/O error occurs
	 */
	public <T> void send(T o) throws SerializationException, IOException {
		this.protocolWrite(new byte[0], o);
	}

	/**
	 * Sends the passed {@link Object} through the Socket given by constructor. The
	 * passed object will be serialized by the corresponding {@link Serializer}.
	 * 
	 * @param header byte[] containing custom header data
	 * @param o      {@link Object} that has to be sent
	 * @throws SerializationException   if no {@link Serializer} is defined for o's
	 *                                  {@link Class}
	 * @throws NoSuchAlgorithmException if the {@link Netprotocol} returns a
	 *                                  checksum algorithm which does not exist
	 * @throws IOException              if an I/O error occurs
	 */
	public <T> void send(byte[] header, T o) throws SerializationException, IOException {
		this.protocolWrite(header, o);
	}

	/**
	 * Reads a package, deserializes it and returns it as {@link Object}. Blocks
	 * current thread. Workaround with isReady method to check if bytes are
	 * available.
	 * 
	 * @return {@link Object} received Object
	 * @throws IOException              if an I/O error occurs
	 * @throws SerializationException   if no {@link Serializer} is defined for o's
	 *                                  {@link Class}
	 * @throws NoSuchAlgorithmException if the {@link Netprotocol} returns a
	 *                                  checksum algorithm which does not exist
	 * @throws BrokenPackageException   if the received package has some corruption
	 *                                  such as a disagreeing checksum
	 */
	public Object retrieve() throws IOException, SerializationException, BrokenPackageException {
		// Read header
		byte[] header = new byte[protocol.getHeaderSize()];
		inputStream.read(header);

		// Read data size
		byte[] dataSize = new byte[protocol.getDataLengthSize()];
		inputStream.read(dataSize);

		// Read checksum
		byte[] remoteChecksum = new byte[protocol.getChecksumSize()];
		inputStream.read(remoteChecksum);

		// Read data
		byte[] data = new byte[ByteBuffer.wrap(dataSize).getInt()];
		inputStream.read(data);

		// Generate local checksum
		MessageDigest md;
		try {
			md = MessageDigest.getInstance(protocol.getChecksumAlgorithm());
			md.update(data);
			byte[] localChecksum = md.digest();

			// Compare checksum
			if (!Arrays.equals(localChecksum, remoteChecksum))
				throw new BrokenPackageException(Registry.CHEKSUM_DIFFER);

			// Deserialize and return
			return protocol.deserialize(new SerializedObject(data));
		} catch (NoSuchAlgorithmException e) {
			throw new EPLIBProtocolException(e);
		}
	}

	/**
	 * Checks if enough bytes are available to read a package header.
	 * 
	 * @return if enough bytes are ready in the stream
	 * @throws IOException
	 */
	public boolean isReady() throws IOException {
		return inputStream
				.available() > (protocol.getHeaderSize() + protocol.getDataLengthSize() + protocol.getChecksumSize());
	}

	/**
	 * Getta for {@link Netprotocol}.
	 * 
	 * @return {@link Netprotocol} used by this {@link Netpipe}
	 */
	public Netprotocol getProtocol() {
		return protocol;
	}

	/**
	 * Getta for {@link InputStream} used to write messages to.
	 * 
	 * @return {@link InputStream} used by this {@link Netpipe}
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Getta for {@link OutputStream} used to retrieve messages from.
	 * 
	 * @return {@link OutputStream} used by this {@link Netpipe}
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * Writes passed data formated to the {@link OutputStream}.
	 * 
	 * @param header byte[] object header
	 * @param o      {@link Object} package content
	 * @throws SerializationException   if no {@link Serializer} is defined for o's
	 *                                  {@link Class}
	 * @throws NoSuchAlgorithmException if the {@link Netprotocol} returns a
	 *                                  checksum algorithm which does not exist
	 * @throws IOException              if an I/O error occurs
	 */
	private void protocolWrite(byte[] header, Object o) throws SerializationException, IOException {
		// Data
		byte[] data = protocol.serialize(o).getCompound();

		// Data size
		byte[] dataSize = ByteBuffer.allocate(protocol.getDataLengthSize()).putInt(data.length).array();

		// Generate checksum
		MessageDigest md;
		try {
			md = MessageDigest.getInstance(protocol.getChecksumAlgorithm());
			md.update(data);
			byte[] checksum = md.digest();

			// Write n' Flush
			outputStream.write(header);
			outputStream.write(dataSize);
			outputStream.write(checksum);
			outputStream.write(data);
			outputStream.flush();
		} catch (NoSuchAlgorithmException e) {
			throw new EPLIBProtocolException(e);
		}
	}

}
