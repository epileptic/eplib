package de.epileptic.eplib.network.serialization;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import de.epileptic.eplib.Registry;

/**
 * Contains a serialized objects data and its identifier for a certain protocol.
 * 
 * @author Epiletic
 *
 */
public class SerializedObject {
	// Object
	private byte[] identifier;
	private byte[] data;
	private byte[] compound;

	/**
	 * Splits the serialized data into its identifier part and its data part.
	 * 
	 * @param raw byte data that has to be split
	 * @throws SerializationException if an serialization error occurs
	 */
	public SerializedObject(byte[] raw) throws SerializationException {
		this.compound = raw;
		if (raw.length >= 4) {
			this.identifier = Arrays.copyOfRange(raw, 0, 4);
			this.data = new byte[0];
			if (raw.length > 4)
				this.data = Arrays.copyOfRange(raw, 4, raw.length);
		} else {
			throw new SerializationException(Registry.INPUT_TOO_SMALL);
		}
	}

	public SerializedObject(byte[] identifier, byte[] data) throws SerializationException {
		this.identifier = identifier;
		this.data = data;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			baos.write(identifier);
			baos.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.compound = baos.toByteArray();
	}

	/**
	 * baos.write(data); Returns the compound of identifier and data. Its equals to
	 * the raw data which was passed to the constructor.
	 * 
	 * @return raw data
	 */
	public byte[] getCompound() {
		return compound;
	}

	/**
	 * Returns the data split from the identifier.
	 * 
	 * @return split data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * Returns the identifier split from the data.
	 * 
	 * @return split identifier
	 */
	public byte[] getIdentifier() {
		return identifier;
	}
}
