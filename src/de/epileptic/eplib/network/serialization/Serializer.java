package de.epileptic.eplib.network.serialization;

import de.epileptic.eplib.network.Netpipe;
import de.epileptic.eplib.network.protocol.Netprotocol;

/**
 * This is designed for {@link Class}es used for serialization and
 * deserialization within the {@link Netpipe}.
 * 
 * @author Epileptic
 *
 */
public interface Serializer<T> {
	public byte[] serialize(T o) throws SerializationException;

	public T deserialize(SerializedObject o, Netprotocol protocol) throws SerializationException;
}
