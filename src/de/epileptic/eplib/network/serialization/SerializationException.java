package de.epileptic.eplib.network.serialization;

public class SerializationException extends Exception {

	private static final long serialVersionUID = -2171922336352986293L;

	public SerializationException(String msg) {
		super(msg);
	}
}
