package de.epileptic.eplib.network.serialization.serializer;

import java.nio.ByteBuffer;

import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;

/**
 * int {@link Serializer}
 * 
 * @author epileptic
 */
public class IntSerializer implements Serializer<Integer> {

	@Override
	public byte[] serialize(Integer o) throws SerializationException {
		return ByteBuffer.allocate(4).putInt((int) o).array();
	}

	@Override
	public Integer deserialize(SerializedObject o, Netprotocol protocol) throws SerializationException {
		return ByteBuffer.wrap(o.getData()).getInt();
	}

}
