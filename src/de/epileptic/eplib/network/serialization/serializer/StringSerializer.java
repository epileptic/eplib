package de.epileptic.eplib.network.serialization.serializer;

import java.io.UnsupportedEncodingException;

import de.epileptic.eplib.Registry;
import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;

public class StringSerializer implements Serializer<String> {

	@Override
	public byte[] serialize(String o) throws SerializationException {
		try {
			return ((String) o).getBytes(Registry.getInstance().DEFAULT_ENCODING);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String deserialize(SerializedObject o, Netprotocol protocol) throws SerializationException {
		try {
			return new String(o.getData(), Registry.getInstance().DEFAULT_ENCODING);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
