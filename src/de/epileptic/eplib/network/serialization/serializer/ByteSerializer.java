package de.epileptic.eplib.network.serialization.serializer;

import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;

/**
 * Byte[] {@link Serializer}
 * 
 * @author epileptic
 *
 */
public class ByteSerializer implements Serializer<byte[]> {

	@Override
	public byte[] serialize(byte[] o) throws SerializationException {
		return o;
	}

	@Override
	public byte[] deserialize(SerializedObject o, Netprotocol protocol) throws SerializationException {
		return o.getData();
	}

}
