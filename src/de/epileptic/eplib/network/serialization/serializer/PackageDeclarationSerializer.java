package de.epileptic.eplib.network.serialization.serializer;

import java.nio.ByteBuffer;

import de.epileptic.eplib.network.packer.PackageDeclaration;
import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;
import de.epileptic.eplib.network.serialization.SerializedObject;
import de.epileptic.eplib.network.serialization.Serializer;

/**
 * {@link PackageDeclaration}
 * 
 * @author epileptic
 *
 */
public class PackageDeclarationSerializer implements Serializer<PackageDeclaration> {

	@Override
	public byte[] serialize(PackageDeclaration o) throws SerializationException {
		return ByteBuffer.allocate(4).putInt(((PackageDeclaration) o).getAmount()).array();
	}

	@Override
	public PackageDeclaration deserialize(SerializedObject o, Netprotocol protocol) throws SerializationException {
		return new PackageDeclaration(ByteBuffer.wrap(o.getData()).getInt());
	}

}
