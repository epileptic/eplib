package de.epileptic.eplib.network.handling;

/**
 * A handler interface handles messages of a specific {@link Class}. Designed
 * for {@link MessageDistributor}.
 * 
 * @author Epileptic
 *
 * @param <T> {@link Class} that is intended to be handled by this
 *            {@link MessageHandler}
 */
public interface MessageHandler<T> {

	/**
	 * Called by {@link MessageDistributor} if a new Message of that specific type
	 * was retrieved.
	 * 
	 * @param o Object of class T
	 */
	public void handle(T o);
}
