package de.epileptic.eplib.network.handling;

import java.util.LinkedHashMap;

/**
 * A tool class used to distribute received objects depending on its
 * {@link Class} to registered handlers. These handlers can be assigned to a
 * specific {@link Class}.
 * 
 * @author Epileptic
 */
public class MessageDistributor {
	private LinkedHashMap<Class<?>, MessageHandler<?>> handler = new LinkedHashMap<Class<?>, MessageHandler<?>>();

	/**
	 * Adds a {@link MessageHandler} to the handler list. It will be used to handle
	 * received Objects of the {@link Class} c.
	 * 
	 * @param <T>     {@link Class} same as cls
	 * @param cls     {@link Class} that has to be handled by the
	 *                {@link MessageHandler} specified in handler
	 * @param handler {@link MessageHandler} that is used to handle the
	 *                {@link Class} specified in cls
	 */
	public <T> void addHandler(Class<T> cls, MessageHandler<T> handler) {
		this.handler.put(cls, handler);
	}

	/**
	 * Handles the passed Object by invoke the handle method of the
	 * {@link MessageHandler} corresponding to the class of the passed Argument
	 * 
	 * @param <T> {@link Class} of the object passed to the handle method
	 * @param o   Object which has to be handled
	 */
	@SuppressWarnings("unchecked")
	public <T> void handle(T o) {
		// Search for class of o
		for (Class<?> i : handler.keySet().toArray(new Class<?>[0])) {
			// Check if current class is class of o
			if (i == o.getClass())
				// Handle o with corresponding handler
				((MessageHandler<T>) handler.get(i)).handle(o);
		}
	}

}
