package de.epileptic.eplib.network;

public class AuthenticationException extends Exception {

	private static final long serialVersionUID = 3547786661221328147L;
	public AuthenticationException(String msg) {
		super(msg);
	}
}
