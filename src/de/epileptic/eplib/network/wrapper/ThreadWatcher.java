package de.epileptic.eplib.network.wrapper;

import java.io.IOException;
import java.net.Socket;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashMap;
import java.util.function.BiConsumer;

import de.epileptic.eplib.log.Log;

public class ThreadWatcher extends Thread {

	LinkedHashMap<Socket, Thread> threads = new LinkedHashMap<Socket, Thread>();

	public ThreadWatcher(LinkedHashMap<Socket, Thread> threads) {
		this.threads = threads;
	}

	@Override
	public void run() {
		while (true) {
			try {
				threads.forEach(new BiConsumer<Socket, Thread>() {
					@Override
					public void accept(Socket t, Thread u) {
						if (!t.isBound() || !t.isConnected() || t.isClosed() || t.isInputShutdown()
								|| t.isOutputShutdown()) {
							try {
								t.close();
							} catch (IOException e) {
							}
							u.interrupt();
							threads.remove(t);
							Log.log("client offline! Thread " + u.getName() + " killed and socket from "
									+ t.getInetAddress() + " closed.", this);
						}
					}
				});
			} catch (ConcurrentModificationException e) {
			}
		}
	}
}
