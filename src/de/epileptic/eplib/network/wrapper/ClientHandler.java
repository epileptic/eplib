package de.epileptic.eplib.network.wrapper;

import java.net.Socket;

public interface ClientHandler extends Runnable {
	public void clientConnect(Socket c);
}
