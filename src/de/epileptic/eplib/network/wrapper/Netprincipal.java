package de.epileptic.eplib.network.wrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.epileptic.eplib.log.Log;

public class Netprincipal extends Thread {

	private ServerSocket server;
	private Class<? extends ClientHandler> handlerClass;

	private ThreadWatcher watcher;

	private LinkedHashMap<Socket, Thread> clients = new LinkedHashMap<Socket, Thread>();
	private ExecutorService service = Executors.newCachedThreadPool();

	public Netprincipal(ServerSocket server, Class<? extends ClientHandler> handler) {
		this.server = server;
		this.handlerClass = handler;

		watcher = new ThreadWatcher(clients);
	}

	@Override
	public void run() {
		watcher.start();
		while (server.isBound()) {
			try {
				Socket c = server.accept();

				ClientHandler temp = handlerClass.newInstance();
				temp.clientConnect(c);
				Log.log("client logged on!", this);

				Thread clientThread = new Thread(temp);

				service.execute(clientThread);
				Log.log("client thread " + clientThread.getName() + " started! " + "client thread "
						+ clientThread.getName() + " created!", this);

				clients.put(c, clientThread);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		try {
			service.shutdown();
			Log.log("executor service shutdown.", this);

			server.close();
			Log.log("Server closed!", this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		watcher.interrupt();
		Log.log("watcher interrupted!", this);
	}
}
