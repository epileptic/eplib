package de.epileptic.eplib.network.wrapper;

import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

import de.epileptic.eplib.network.AuthenticationException;
import de.epileptic.eplib.network.BrokenPackageException;
import de.epileptic.eplib.network.handling.MessageDistributor;
import de.epileptic.eplib.network.packer.Packpipe;
import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;

public class Networker {
	public Packpipe pipe;

	private MessageDistributor distributor;

	public Networker(Socket s, Netprotocol protocol)
			throws IOException, NoSuchAlgorithmException, SerializationException, AuthenticationException {
		pipe = new Packpipe(s.getInputStream(), s.getOutputStream(), protocol);
		distributor = new MessageDistributor();
	}

	/**
	 * Blocks
	 * 
	 * @throws BrokenPackageException
	 * @throws SerializationException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public void handle() throws NoSuchAlgorithmException, IOException, SerializationException, BrokenPackageException {
		Object obj = pipe.retrieve();
		distributor.handle(obj);
	}

}
