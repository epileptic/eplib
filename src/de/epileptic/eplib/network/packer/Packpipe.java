package de.epileptic.eplib.network.packer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import de.epileptic.eplib.network.AuthenticationException;
import de.epileptic.eplib.network.BrokenPackageException;
import de.epileptic.eplib.network.Netpipe;
import de.epileptic.eplib.network.protocol.Netprotocol;
import de.epileptic.eplib.network.serialization.SerializationException;

/**
 * A specialization of {@link Netpipe} which allows to send multiple Objects
 * through a {@link Netpipe}.
 * 
 * @author Epileptic
 *
 */
public class Packpipe extends Netpipe {

	public Packpipe(InputStream is, OutputStream os, Netprotocol protocol)
			throws IOException, NoSuchAlgorithmException, SerializationException, AuthenticationException {
		super(is, os, protocol);
	}

	/**
	 * Generates a {@link PackageDeclaration} and sends passed Objects with their
	 * corresponding header through a {@link Netpipe}
	 * 
	 * @param header byte[][] with headers corresponding to the passed Objects.
	 * @param o      Objects that has to be sent
	 * @throws NoSuchAlgorithmException if protocol hash or data checksum *
	 *                                  algorithms can't be found.
	 * @throws SerializationException   if a error occurs in {@link Object}
	 *                                  serialization.
	 * @throws IOException              if an I/O error occurs.
	 */
	public void send(byte[][] header, Object... o)
			throws NoSuchAlgorithmException, SerializationException, IOException {

		// Create package declaration if more then one
		if (header.length > 1) {
			PackageDeclaration d = new PackageDeclaration(o.length);
			super.send(d);
		}

		// Send objects
		for (int i = 0; i < o.length; i++)
			if (header.length >= i)
				super.send(header[i], o[i]);
			else
				super.send(o[i]);
	}

	/**
	 * Generates a {@link PackageDeclaration} and sends passed Objects without
	 * headers through a {@link Netpipe}
	 * 
	 * @param o Objects that has to be sent
	 * @throws NoSuchAlgorithmException if protocol hash or data checksum *
	 *                                  algorithms can't be found.
	 * @throws SerializationException   if a error occurs in {@link Object}
	 *                                  serialization.
	 * @throws IOException              if an I/O error occurs.
	 */
	public void send(Object... o) throws SerializationException, NoSuchAlgorithmException, IOException {
		// Create package declaration if more then one
		if (o.length > 1) {
			PackageDeclaration d = new PackageDeclaration(o.length);
			super.send(d);
		}

		// Send Objects
		for (Object i : o)
			super.send(i);
	}

	/**
	 * Retrieve a Object through this {@link Netpipe}. Returns Object if only one
	 * was sent and Object[] if multiple were sent
	 */
	@Override
	public Object retrieve() throws IOException, SerializationException, BrokenPackageException {
		Object o = super.retrieve();

		if (!(o instanceof PackageDeclaration))
			return o;

		// TODO timeout
		ArrayList<Object> objects = new ArrayList<Object>();
		for (int i = 0; i < ((PackageDeclaration) o).getAmount(); i++) {
			objects.add(super.retrieve());
		}

		return (Object[]) objects.toArray(new Object[objects.size()]);
	}
}
