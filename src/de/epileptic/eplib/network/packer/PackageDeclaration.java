package de.epileptic.eplib.network.packer;

import de.epileptic.eplib.network.Netpipe;
import de.epileptic.eplib.network.protocol.Netprotocol;

/**
 * This class is natively serializable by every {@link Netprotocol}. It is used
 * to declare a Package containing multiple Objects in the {@link Packpipe}
 * {@link Netpipe}. It simply declares how many Objects are contained in this
 * specific package.
 * 
 * @author Epileptic
 *
 */
public class PackageDeclaration {
	int amount = 2;

	/**
	 * Constructs a {@link PackageDeclaration} which defines a package with [amount]
	 * Objects.
	 * 
	 * @param amount int defining the size of the package
	 */
	public PackageDeclaration(int amount) {
		this.amount = amount;
	}

	/**
	 * Returns the amount of the contained packages.
	 * 
	 * @return
	 */
	public int getAmount() {
		return amount;
	}
}
