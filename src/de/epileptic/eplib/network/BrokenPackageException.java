package de.epileptic.eplib.network;

public class BrokenPackageException extends Exception {

	private static final long serialVersionUID = 3547786661221328147L;
	public BrokenPackageException(String msg) {
		super(msg);
	}
}
